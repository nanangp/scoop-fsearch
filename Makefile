## For development

all: ci-pkg-build
check:
	$(MAKE) ci-chk-lint
	$(MAKE) ci-chk-test COV_ENV=
license: ci-pkg-build-license
package: ci-pkg-package
.PHONY: all check license package

## For CI

APPNAME = scoop-fsearch
HOST = x86_64-unknown-linux-musl
TARGET = x86_64-pc-windows-gnu

CARGO_ABOUT_VER = 0.6.2
CARGO_ABOUT_HASH = bb5ae5d2ab339b0d3163ec7c7e73599bb75b69040b850034bbaa2a773feae409

COV_ENV = RUSTFLAGS="-C instrument-coverage" LLVM_PROFILE_FILE="target/coverage/%p-%m.profraw"
GRCOV_URL = https://github.com/mozilla/grcov/releases/latest/download/grcov-$(HOST).tar.bz2

# check

ci-chk-deps: ci-chk-deps-os ci-chk-deps-rust ci-chk-deps-cov
ci-chk-deps-os:
	time apk add libxml2-utils
ci-chk-deps-rust:
	time rustup component add clippy llvm-tools rustfmt
ci-chk-deps-cov:
	time sh -c 'wget -O - "$(GRCOV_URL)" | tar -xjf -'
	mv grcov /usr/local/bin/grcov
.PHONY: ci-chk-deps ci-chk-deps-cov ci-chk-deps-os ci-chk-deps-rust

ci-chk-lint: ci-chk-lint-fmt ci-chk-lint-clippy
ci-chk-lint-fmt:
	cargo fmt -- --check
ci-chk-lint-clippy:
	time cargo clippy -- -D warnings
.PHONY: ci-chk-lint ci-chk-lint-clippy ci-chk-lint-fmt

ci-chk-test: ci-chk-test-build ci-chk-test-run-unit ci-chk-test-run-integration
ci-chk-test-build:
	$(COV_ENV) time cargo build
ci-chk-test-run-unit: ci-chk-test-build
	$(COV_ENV) time cargo test
ci-chk-test-run-integration: ci-chk-test-build
	$(COV_ENV) time ./test.sh
.PHONY: ci-chk-test ci-chk-test-build ci-chk-test-run-unit ci-chk-test-run-integration

ci-chk-cov: ci-chk-cov-cobertura ci-chk-cov-html
ci-chk-cov-cobertura:
	grcov target/coverage --binary-path target/debug -s . -o target/coverage.xml -t cobertura --keep-only 'src/*'
	xmllint --xpath "concat('Coverage: ', 100 * string(//coverage/@line-rate), '%')" target/coverage.xml
ci-chk-cov-html:
	grcov target/coverage --binary-path target/debug -s . -o target/coverage -t html
.PHONY: ci-chk-cov ci-chk-cov-cobertura ci-chk-cov-html

ci-chk-all:
	$(MAKE) ci-chk-deps
	$(MAKE) ci-chk-lint
	$(MAKE) ci-chk-test
	$(MAKE) ci-chk-cov
.PHONY: ci-chk-all

# package

ci-pkg-deps: ci-pkg-deps-os ci-pkg-deps-rust ci-pkg-deps-license
ci-pkg-deps-os:
	time apk add 7zip git mingw-w64-gcc
ci-pkg-deps-rust: ci-pkg-deps-os
	time rustup target add "$(TARGET)"
	time rustup component add rust-src
ci-pkg-deps-license:
	CARGO_ABOUT_VER="$(CARGO_ABOUT_VER)" CARGO_ABOUT_HASH="$(CARGO_ABOUT_HASH)" HOST="$(HOST)" \
	  tools/ci-install-cargo-about.sh
.PHONY: ci-pkg-deps ci-pkg-deps-license ci-pkg-deps-os ci-pkg-deps-rust

ci-pkg-build: ci-pkg-build-rust ci-pkg-build-license ci-pkg-build-shims
ci-pkg-build-rust:
	time cargo +nightly build --target "$(TARGET)" -Z build-std=panic_abort,std -Z build-std-features=panic_immediate_abort --release --all
	wc -c "target/$(TARGET)/release/"*.exe
	sha256sum "target/$(TARGET)/release/"*.exe
ci-pkg-build-license:
	-mkdir target
	cargo about generate -o target/LICENSE.html data/license-template.hbs
ci-pkg-build-shims:
	-mkdir target
	tools/ci-build-shims.sh
.PHONY: ci-pkg-build ci-pkg-build-license ci-pkg-build-rust ci-pkg-build-shims

ci-pkg-package:
	APPNAME="$(APPNAME)" TARGET="$(TARGET)" tools/ci-package.sh
.PHONY: ci-pkg-package

ci-pkg-upload:
	APPNAME="$(APPNAME)" tools/ci-upload.sh "$(APPNAME)"-*.7z "$(APPNAME)"-*.zip
.PHONY: ci-pkg-upload

ci-pkg-all:
	$(MAKE) ci-pkg-deps
	$(MAKE) ci-pkg-build
	$(MAKE) ci-pkg-package
	if [ -n "$(CI_COMMIT_TAG)" ]; then $(MAKE) ci-pkg-upload; fi
.PHONY: ci-pkg-all
