#!/bin/sh
set -ex

test -n $CI
test -n $APPNAME

for file in "$@"; do
	time wget --header "JOB-TOKEN: $CI_JOB_TOKEN" --post-file "$file" \
		"$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/generic/$APPNAME/$CI_COMMIT_TAG/$(basename "$file")"
done
