//! `scoop search` alternative.

fn main() {
    let result = scoop_fsearch::main(std::env::args_os(), |k| std::env::var_os(k), None);
    if let Err(err) = result {
        use std::io::Write;
        debug_assert!(err.ends_with('\n'));
        let _ = std::io::stderr().write_all(err.as_bytes());
        std::process::exit(1);
    }
}
