#!/bin/sh

set -e

datadir=$(dirname "$0")/data
homedir=$datadir/test-home
scoopdir=$homedir/scoop
nonexistentdir=$datadir/test-nonexistent

${CARGO:-cargo} build --quiet
scoop_fsearch="target/debug/scoop-fsearch"
scoop_manifest="target/debug/scoop-manifest"

set -x

# TODO: These should probably be unit tests instead

# SCOOP takes precedence
SCOOP="$scoopdir" USERPROFILE="$nonexistentdir" $scoop_fsearch basic > /dev/null
! SCOOP="$nonexistentdir" USERPROFILE="$homedir" $scoop_fsearch basic 2> /dev/null

# When SCOOP is empty, use $USERPROFILE/scoop
SCOOP= USERPROFILE="$homedir" $scoop_fsearch basic > /dev/null
! SCOOP= USERPROFILE="$nonexistentdir" $scoop_fsearch basic 2> /dev/null

# Simple tests for scoop-manifest
SCOOP="$scoopdir" $scoop_manifest basic > /dev/null
! SCOOP="$scoopdir" $scoop_manifest nonexistent 2> /dev/null
